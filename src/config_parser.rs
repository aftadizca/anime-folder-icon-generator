use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub api: Api,
    pub img: Img,
    pub path: MyPath,
}

#[derive(Debug, Deserialize)]
pub struct MyPath {
    pub anime: Vec<String>,
    pub exclude: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct Api {
    pub query: String,
    pub url: String,
}

#[derive(Debug, Deserialize)]
pub struct Img {
    pub top: String,
    pub bottom: String,
    pub coordinate: Vec<u32>,
}
